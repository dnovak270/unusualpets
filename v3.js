// REQUIRE MODULES
const http = require('http');
const https = require('https');
const logger = require('morgan');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const path = require('path');
const hbs = require('hbs');
const expressValidator = require('express-validator');
const fs = require('fs');

// AUTH MODULES
const session = require('express-session');
const passport = require('passport');
const MongoStore = require('connect-mongo')(session);
const LocalStrategy = require('passport-local').Strategy;
const flash = require('connect-flash');

//REQUIRE FILES
const router = require('./routes_v3-v5');
const mongoConnection = require('./connect.js');
const User = require('./models/User');

// USE GZIP COMPRESSION
const compression = require('compression');
app.use(compression());

// SET VIEW ENGINE
app.set('views', path.join(__dirname, 'views-v3'));
app.set('view engine', 'hbs');
hbs.registerPartials(__dirname + '/views-v3/partials');

// USING ASSETS FOLDER FOR STATIC FILES
app.use(express.static(path.join(__dirname, 'assets-min'),{ etag: true, maxAge: 31536000 }));

// USE PARSERS
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressValidator());
app.use(cookieParser());

// SESSION
app.use(session({
    store: new MongoStore({
        mongooseConnection: mongoConnection,
        ttl: 14*24*60*60 
    }),
    secret: 'xsoefiwfg',
    resave: false,
    saveUninitialized: false,
    // cookie: { secure: true }
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

passport.serializeUser(function(user, done) {
    done(null, user._id);
});
  
passport.deserializeUser(function(id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

app.use(function(req, res, next) {
    res.locals.error = req.flash('error');
    res.locals.user = req.user;
    res.locals.isAuthenticated = req.isAuthenticated();
    next();
});

passport.use(new LocalStrategy(
    function(username, password, done) {
        User.findOne({ username: username }).then((user) => {
            if (!user) { 
                console.log("user does not exist"); 
                return done(null, false, {message: 'User does not exist'});  
            } else {
                user.verifyPassword(password, function(err, isMatch) {
                    if (err) throw err;
                    
                    if(isMatch) { 
                        console.log("password match"); 
                        return done(null, user); 
                    } else { 
                        console.log("password does not match"); return done(null, false, {message: 'Invalid password.'}); 
                    }
                });
            }
        }).catch((err) => done(err));
    }
));

// USE SERVER LOGGER
//app.use(logger('dev'));s

// ROUTES
app.use(router);

// STARTING SERVERS
let httpServer = http.createServer(app).listen(8080, '0.0.0.0', (err)=>{
    if (err) {
        throw new Error(err);
    } else {
        console.log(`HTTP server is listening on <http://localhost:8080/>`);
    }
});