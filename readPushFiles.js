const fs = require('fs');
const path = require('path');
const url = require('url');

let files = {}
fs.readdir('Push', (error, data)=>{
  data.forEach(name=>{
    files[`${name}`]=fs.readFileSync(path.join(__dirname, 'Push', `${name}`), {encoding: 'utf8'})
    .split('\n')
    .filter(line=>line.match(/src *?= *?"(.*)"/)!=null)
    .map(line=>line.match(/src *?= *?"(.*)"/)[1])
    
  });
  console.log(files);
});


