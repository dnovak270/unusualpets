// REQUIRE MODULES
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

// mongo URL
const mongoURL = `mongodb://localhost/unusualpets`;
//const mongoURL = 'mongodb://dnovak270:4YUDmyS8@ds247347.mlab.com:47347/unusualpets'

// CONNECT TO mongoDB
const connection = mongoose.createConnection(mongoURL, {useMongoClient:true});

connection.on('error', (err) => console.log(err.message));
connection.once('open', () => console.log("Connection to database established!"));

module.exports = connection;




