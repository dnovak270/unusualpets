$(document).ready(function() {
    $('.btn-favourite').click(function(){
        var URL = window.location.pathname;
        var petId = URL.substring(URL.lastIndexOf('/') + 1);
        var ajaxURL = "/pet/"+petId+"/favourite";
        $.ajax({
            url: ajaxURL,
            success: function(data) {
                $('.fav-msg').text(data.msg);
                var favNumber = parseInt($('.pet-favs').text());
                if(data.success){
                    $('.pet-favs').text(favNumber+1);
                }
            }
        });
    });

    $('.btn-remove-fav').click(function(){
        var btnRemoveFav = $(this);
        var petId = $(this).val();
        var ajaxURL = "/pet/"+petId+"/remove";
        $.ajax({
            url: ajaxURL,
            success: function(data) {
                if(data.success){
                    $('.fav-msg').html(data.msg);
                    btnRemoveFav.parent().remove();
                }
            }
        });
    });

    $('.btn-new-form').click(function(){
        $('.alert-danger').remove();
        $('.form-column').toggle();
        $(window).scrollTop($(document).height()-$(window).height());
    });

});