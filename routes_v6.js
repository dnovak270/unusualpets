// REQUIRE MODULES
const express = require('express');
const passport = require('passport');
const router = express.Router();
const multer = require('multer');

// FILE UPLOAD
const storage = multer.diskStorage({
    destination: './assets-min/img/',
    filename: function (req, file, cb) {
      cb(null, file.fieldname + '-' + Date.now() + '.jpg');
    }
}); 
const upload = multer({ storage: storage });

// REQUIRE CONTROLLERS
const IndexController = require('./controllers/IndexControllerPush');
const UserController = require('./controllers/UserController');
const PetController = require('./controllers/PetController');
const CommentController = require('./controllers/CommentController');

// INDEX ROUTE 
router.get('/', IndexController.index); 

router.get('/cats',  IndexController.cat);

router.get('/dogs',  IndexController.dog);

router.get('/other',  IndexController.other);

router.get('/imprint', IndexController.imprint); 
// USER ROUTES
router.get('/user/register', UserController.register_get);

router.post('/user/register', UserController.register_post);

router.get('/user/login', UserController.login_get);

router.post('/user/login', passport.authenticate('local', { failureRedirect: '/user/login', failureFlash: true }) , UserController.login_post);

router.get('/user/profile', UserController.user_profile);

router.get('/user/logout', UserController.logout);

// PET ROUTES
router.post('/pet/create', upload.single('img'), PetController.pet_create);

router.get('/pet/:id',  PetController.pet_detail);

router.get('/pet/:id/delete',  PetController.pet_delete);

router.get('/pet/:id/favourite', PetController.add_favourite);

router.get('/pet/:id/remove', PetController.remove_favourite);

// COMMENT ROUTES
router.post('/comment/:id', CommentController.comment_create);

// EXPORTS
module.exports = router;