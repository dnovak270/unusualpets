// REQUIRE MODULES
const mongoose = require('mongoose');
const Pet = require('../models/Pet');
const Comment = require('../models/Comment');
const User = require('../models/User');
const fs = require('fs');
const path = require('path');
const MAX_PETS = 10;

exports.pet_create =  function(req, res) {
    if(req.isAuthenticated()){
        let imgFilename;
        if (req.file){
            imgFilename = req.file.filename;
        }
        req.checkBody('name', 'Invalid pet name.').len(2,50);
        req.checkBody('class', 'Please select your pet class.').isIn(['Cat','Dog','Other']);
        req.checkBody('sex', 'Please select your pet sex').isIn(['male','female']);
        req.checkBody('toy', 'Invalid toy name.').len(0,100);
        req.checkBody('food', 'Invalid food name.').len(0,100);
        req.checkBody('age').toInt();
        
        let errors = req.validationErrors();
        if (errors) {
            req.flash('errors', errors);
            res.redirect('/user/profile');
        } else {
            Pet.find({owner:req.user._id}).count((err, result) => {
                if (result < MAX_PETS) {
                    Pet.create({
                        name:req.body.name, 
                        class:req.body.class,
                        age:req.body.age,
                        sex:req.body.sex, 
                        food:req.body.food,
                        toy:req.body.toy,
                        img:imgFilename, 
                        owner:req.user._id
                    }).then((pet)=>{
                        console.log(pet);
                        res.redirect(`/pet/${pet._id}`);
                    }).catch((err) => {
                        req.flash('errors', [{ msg: 'Error adding your pet.'}]);
                        res.redirect('/user/profile');
                    });
                } else {
                    req.flash('errors', [{ msg: 'Your profile has maximum number of pets.'}]);
                    res.redirect('/user/profile');
                }
            });
        }      
    } else {
        res.redirect('/user/login');
    }
}

exports.pet_detail = function(req, res) {
    Pet.findById(req.params.id).then((pet) => {
        User.findById(pet.owner).then((user) => {
            Comment.find({pet:pet._id}).then((comments) => {
                //let comments = [{text: "blyat", timestamp: Date.now()}];
                res.render('pet.hbs', {pet: pet, owner:user.username, comments: comments});
            });
        });
 
    }).catch((err) => {
        res.redirect('/user/profile');
    });  
}

exports.pet_delete = function(req,res) {
    if(req.isAuthenticated()){
        Pet.findOneAndRemove({_id:req.params.id, owner:req.user._id}).then((pet) => {
            let imgPath = path.join(__dirname, `/../assets/img/${pet.img}`);
            fs.stat(imgPath, function (err, stats) {
                if (err) {
                    console.log('There is no image to delete.');
                } else {
                    fs.unlink(imgPath, function(err){
                        if(err) console.log(err);
                   }); 
                }
                res.redirect('/user/profile');
            });
        }).catch((err) => {
            req.flash('errors', [{ msg: 'Error deleting your pet.'}]);
            res.redirect('/user/profile');
        });
    } else {
        res.redirect('/user/login');
    }
}

exports.add_favourite = function(req, res) {
    if(req.isAuthenticated()){
        let userId = mongoose.Types.ObjectId(req.user._id);
        Pet.findById(req.params.id).then((pet) => {        
            let msg;
            let isFav = pet.favs.some(function (fav) {
                return fav.equals(userId);
            });
            
            if(isFav) {
                success = false;
                msg = `${pet.name} is already your favourite pet!`
            } else {
                pet.favs.push(userId);
                pet.save();
                success = true;
                msg = `Congratulations, ${pet.name} is now your favourite pet!`
            }
            res.json({msg: msg, success: success});

        }).catch((err) => { 
            if(err) console.log(err);
            res.redirect('/');
        });
    }
}


exports.remove_favourite = function(req, res) {
    if(req.isAuthenticated()){
        let userId = mongoose.Types.ObjectId(req.user._id);
        Pet.findById(req.params.id).then((pet) => {        
            let msg = "";
            let success = false;
            let isFav = pet.favs.some(function (fav) {
                if(fav.equals(userId)){
                    pet.favs.remove(userId);
                    pet.save();
                    success = true;
                    msg = `<a href="/pet/${pet._id}" >${pet.name}</a> is no longer your favourite pet!`
                } 
            });

            res.json({msg: msg, success: success});
        }).catch((err) => { 
            if(err) console.log(err);
            res.redirect('/');
        });
    }
}