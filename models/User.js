// REQUIRE MODULES
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const mongoConnection = require('../connect.js');

const Schema = mongoose.Schema;

// CREATE USER SCHEMA
const UserSchema = new Schema({
    username: {
        type:String,
        required: [true, 'This field is requiered'],
        index: { unique: true }
    },
    email: {
        type:String,
        required: [true, 'This field is requiered'],
        index: { unique: true }
    },
    password: {
        type:String,
        required: [true, 'This field is requiered']
    }

});

// HASH PASSWORD BEFORE SAVE
UserSchema.pre('save', function(next) {
    const user = this;

    bcrypt.genSalt(saltRounds, function(err, salt) {
        if (err) return next(err);

        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            user.password = hash;
            next();
        });
    });
});

// SCHEMA METHODS
UserSchema.methods.verifyPassword = function(password, cb) {
    bcrypt.compare(password, this.password, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

// CREATE USER MODEL
const User = mongoConnection.model('User', UserSchema);

// EXPORTS
module.exports = User;