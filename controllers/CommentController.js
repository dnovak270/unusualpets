// REQUIRE MODULES
const User = require('../models/User');
const Pet = require('../models/Pet');
const Comment = require('../models/Comment');

exports.comment_create =  function(req, res) {
    if(req.isAuthenticated()){
        req.checkBody('comment', 'Comment too long.').len(1,350);
        let errors = req.validationErrors();
        if (!errors) {
            let timestamp = new Date();
            Comment.create({
                text: req.body.comment,
                timestamp: `${timestamp.getDate()}/${timestamp.getMonth()+1}/${timestamp.getFullYear()}`, 
                pet: req.params.id, 
                byuser: req.user.username}).then((comment)=>{
                res.redirect(`/pet/${req.params.id}`);
            });
        } else {
            res.redirect(`/pet/${req.params.id}`);
        }
        
    } else {
        res.redirect('/user/login');
    }
}

