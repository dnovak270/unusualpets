// REQUIRE MODULES
const User = require('../models/User');
const Pet = require('../models/Pet');
const passport = require('passport');

const MAX_USERS = 10;

exports.register_get = function(req, res, next) {
    res.render('register.hbs');
};

exports.register_post = function(req, res, next) {
    req.checkBody('username', 'Username must be between 4-15 characters long.').len(4,15);
    req.checkBody('email', 'The email you have entered is not valid, please try again.').isEmail();
    req.checkBody('email', 'Email adress must be between 4-100 characters long.').len(4,100);
    req.checkBody('password', 'Password must be between 4-15 characters long.').len(4,15);
    req.checkBody('confirm', 'Passwords do not match, please try again').equals(req.body.password);

    const errors = req.validationErrors();

    if (errors) {
        res.render('register.hbs', {errors: errors});
    } else {
        User.find().count((err, result) => {
            if (result < MAX_USERS) {
                User.create(req.body).then((user)=>{
                    console.log(`${user.username} has registered!`);
        
                    req.login(user, function(err){
                        res.redirect('/');
                    }); 
        
                }).catch((err) => {
                    let msg = null;
                    if(err.name === "MongoError" && err.code === 11000) {
                        msg = 'Username or email already in use!';
                    } else {
                        msg = 'Erorr, please try again.';
                    }
                    res.render('register.hbs',{errors: [{ msg: msg}]});
                });
            } else {
                res.render('register.hbs', {errors: [{ msg: 'Maximum number of users has been registered.'}]});
            }
        });
    }
};

exports.user_profile = function(req, res) {
    if(req.isAuthenticated()){
        let userPets, favPets;
        Pet.find({owner: req.user._id}).then((uPets) => {
            userPets = uPets;
            Pet.find({favs: req.user._id}).then((fPets) => {
                favPets = fPets;
                res.render('profile.hbs', {favPets: favPets, userPets: userPets, errors: req.flash('errors')});
            }).catch((err) => { 
                if(err) console.log(err);
                res.redirect('/');
            });
            
        }).catch((err) => { 
            if(err) console.log(err);
            res.redirect('/');
        });
    } else {
        res.redirect('/user/login');
    }
};

exports.login_get = function(req, res) {
    res.render('login.hbs');
};

exports.login_post = function(req, res) {
    res.redirect('/user/profile');
};

exports.logout = function(req, res) {
    req.logout(); 
    req.session.destroy(() => { 
        res.clearCookie('connect.sid');
        res.redirect('/');
    });
};
