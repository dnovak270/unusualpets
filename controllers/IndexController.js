// REQUIRE MODULES
const User = require('../models/User');
const Pet = require('../models/Pet');
const passport = require('passport');

exports.index = function(req, res) {
    Pet.find({}).then((pets) => {
        pets.sort(function(obj1, obj2) {
            return obj2.favs.length - obj1.favs.length;
        }).splice(4);
        res.render('index.hbs', {pets: pets});
    }).catch(() => {
        if(err) console.log(err);
    });
 
};

exports.imprint = function(req, res) {
    res.render('imprint.hbs');
};

exports.cat = function(req, res) {
    Pet.find({class:'Cat'}).then((pets) => {
        pets.sort(function(obj1, obj2) {
            return obj2.favs.length - obj1.favs.length;
        });
        res.render('index.hbs', {pets: pets});
    }).catch(() => {
        if(err) console.log(err);
    });

}

exports.dog = function(req, res) {
    Pet.find({class:'Dog'}).then((pets) => {
        pets.sort(function(obj1, obj2) {
            return obj2.favs.length - obj1.favs.length;
        });
        res.render('index.hbs', {pets: pets});
    }).catch(() => {
        if(err) console.log(err);
    });

}

exports.other = function(req, res) {
    Pet.find({class:'Other'}).then((pets) => {
        pets.sort(function(obj1, obj2) {
            return obj2.favs.length - obj1.favs.length;
        });
        res.render('index.hbs', {pets: pets});
    }).catch(() => {
        if(err) console.log(err);
    });


}