// REQUIRE MODULES
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoConnection = require('../connect.js');

// CREATE COMMENT SCHEMA
const CommentSchema = new Schema({
    text: {
        type:String,
        required: true
    },
    timestamp: {
        type:String,
    },
    pet: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pet'
    },
    byuser: {
        type:String,
        required: true
    } 
});

// CREATE PET MODEL
const Comment = mongoConnection.model('Comment', CommentSchema);

// EXPORTS
module.exports = Comment;