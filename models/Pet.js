// REQUIRE MODULES
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoConnection = require('../connect.js');

// CREATE PET SCHEMA
const PetSchema = new Schema({
    name: {
        type:String,
        required: true
    },
    class: {
        type:String,
        enum : ['Dog','Cat','Other'],
        required: true
    },
    age:{
        type:Number
    },
    sex: {
        type:String,
        enum: ['male','female'],
        required: true
    },
    food:{
        type:String
    },
    toy:{
        type:String
    },
    img: {
        type:String
    },
    owner: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    favs: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }] 
});

// CREATE PET MODEL
const Pet = mongoConnection.model('Pet', PetSchema);

// EXPORTS
module.exports = Pet;